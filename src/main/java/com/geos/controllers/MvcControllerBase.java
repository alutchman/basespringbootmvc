package com.geos.controllers;


import com.geos.utils.EmailConceptAsPdf;
import com.geos.utils.PdfBytesProducer;
import com.geos.xmldata.Company;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.xml.transform.StringResult;

import javax.servlet.http.HttpSession;
import javax.xml.bind.Marshaller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MvcControllerBase  {

    private static final Logger LOGGER = LoggerFactory.getLogger(MvcControllerBase.class);

    @Autowired
    private PdfBytesProducer pdfBytesProducer;

    @Autowired
    private EmailConceptAsPdf emailConceptAsPdf;

    @Autowired
    private Jaxb2Marshaller jaxb2Marshaller;



    @RequestMapping("/")
    public String home(Model model) {
        List<String> features = new ArrayList<>();
        features.add("Spring Boot with multiple Controllers");
        features.add("JSP view with CSS, Images and Javascript");
        features.add("Error view using ErrorController");
        features.add("Server Port can be set in the resources `application.properties` by adding: server.port = 8090 [default=8080 when not set]");
        features.add("Contextpath will be root by default: http://localhost:8080/");
        features.add("To get a path like http://localhost:8080/mycontext, your will need to" +
                " add the property:`server.contextPath=/mycontext` to the resources `application.properties` ");
        features.add("To get the context in your jsp use:  ${pageContext.servletContext.contextPath}");
        model.addAttribute("features", features);
        return "system/start";
    }


    @RequestMapping(value="/contract/email.html", method = {RequestMethod.GET})
    public ModelAndView pdf(HttpServletRequest request, HttpSession sessionl)  {
        Map<String, Object> datamap = new HashMap<String, Object>();
        List<Integer> wages = new ArrayList<>();
        for (int wage=2000; wage<=4800; wage+=100) {
            wages.add(wage);
        }

        datamap.put("wage", wages);
        datamap.put("werkuren", new int[]{40,36, 32});
        datamap.put("vakantiedagen", new int[]{20,25,30,35, 40});

        return new ModelAndView("system/pdfEmail", datamap);
    }

    @RequestMapping(value="/contract/email.html", method = {RequestMethod.POST})
    public String pdfReceivePost(HttpServletRequest request, @RequestParam Map<String,String> params)  {
        byte[] contents = pdfBytesProducer.getPDF(params, request);

        emailConceptAsPdf.email(contents, "arwlutchman@gmail.com");
        return "system/pdfMailSent";
    }

    @RequestMapping(value="/contract/page{\\d+}.html", method = {RequestMethod.POST})
    public ModelAndView getContractPage(@RequestParam Map<String,String> params, @RequestParam("jspPage") String newView){
        return new ModelAndView(newView, params);
    }


    @RequestMapping(value="example.pdf", method=RequestMethod.GET)
    public ResponseEntity<byte[]> getPDF(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>();

        params.put("firstname","Dagobert");
        params.put("lastname","Duck");
        params.put("wage","3000");
        params.put("aanhef","De heer");
        params.put("werkuren","40");
        params.put("ondertekendatum","15 november 2016");
        params.put("aanvangdatum","1 december 2016");
        params.put("geboorteDatum","1 januari 1980");
        params.put("vakantiedagen","25");

        byte[] contents = pdfBytesProducer.getPDF(params, request);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        String filename = "conceptContract.pdf";
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
        return response;
    }

    @ResponseBody
    @RequestMapping(value="company.xml", method=RequestMethod.GET)
    private String exampleXml(){
        try {
            Company acompany = new Company();
            acompany.setCompanyName("Dpa GEOS");
            acompany.setNoEmp(35);
            acompany.setCeoName("Fred Boeve");
            StringResult result = new StringResult();


            jaxb2Marshaller.marshal(acompany, result);
            return result.toString();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }


    }


}
