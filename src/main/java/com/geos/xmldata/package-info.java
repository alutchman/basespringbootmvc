@XmlSchema(
        namespace = "http://www.dpa.nl",
        elementFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix="dpa", namespaceURI="http://www.dpa.nl")
        }
)
package com.geos.xmldata;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;