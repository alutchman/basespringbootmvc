package com.geos.utils;

import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class MiscCalculations {

    /**
     * Block for singleton
     */
    private MiscCalculations(){

    }


    public static String getBaseUrlFromRequest(HttpServletRequest request){
        String baseUrl = request.getScheme() + "://" +
                request.getServerName() +
                ("http".equals(request.getScheme())  && request.getServerPort() == 80 ||
                 "https".equals(request.getScheme()) && request.getServerPort() == 443 ? "" : ":" +
                  request.getServerPort() )+request.getContextPath();
        return baseUrl;
    }


    /**
     *
     * @param uri
     * @param params
     * @return
     */
    public static ResponseEntity<String> postMultivalueMapToUrl(String uri, MultiValueMap<String, String> params){
        RestTemplate restTemplate = new RestTemplate();

        HttpMessageConverter<?> formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter<?> stringHttpMessageConverter = new StringHttpMessageConverter();
        List<HttpMessageConverter> msgConverters = new ArrayList<HttpMessageConverter>();
        msgConverters.add(formHttpMessageConverter);
        msgConverters.add(stringHttpMessageConverter);

        // Prepare acceptable media type
        List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
        acceptableMediaTypes.add(MediaType.ALL);

        // Prepare header
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(acceptableMediaTypes);
        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(params,headers);

        ResponseEntity<String> result = restTemplate.postForEntity(uri, httpEntity, String.class);
        return result;
    }


    /**
     *
     * @param uri
     * @param params
     * @return
     */
    public static ResponseEntity<String> postHashMapToUrl(String uri, Map<String, String> params){
        MultiValueMap<String, String> paramsMultiValue = new LinkedMultiValueMap<>();
        paramsMultiValue.setAll(params);

        return postMultivalueMapToUrl(uri, paramsMultiValue);
    }
}
