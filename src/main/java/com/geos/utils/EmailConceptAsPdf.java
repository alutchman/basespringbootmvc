package com.geos.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

@PropertySource("classpath:mail.properties")
@Component
public class EmailConceptAsPdf {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailConceptAsPdf.class);


    @Value("${senderName}")
    private String senderName;


    @Value("${sender}")
    private String sender;

    @Value("${smtpHost}")
    private String smtpHost;

    @Value("${smtpPort}")
    private int smtpPort;

    @Value("${mailUser}")
    private String mailUser;

    @Value("${mailAccess}")
    private String mailAccess;

    /**
     * Sends an email with a PDF attachment.
     */
    public void email(byte[] bytesForPdf,  String recipient) {
        String content = "dummy content"; //this will be the text of the email
        String subject = "dummy subject"; //this will be the subject of the email

        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", smtpHost);
        properties.put("mail.smtp.port", smtpPort);

        properties.put("mail.smtp.user", mailUser);
        properties.put("mail.smtp.password", mailAccess);

        LOGGER.info("Sender name and email= {} {}",System.getProperty("SenderName") , System.getProperty("SenderEmail"));
        Session session = Session.getInstance(properties, new SMTPAuthenticator(mailUser, mailAccess));

        try {
            //construct the text body part
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText(content);

            //construct the mime multi part
            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(textBodyPart);


            if (bytesForPdf != null) {
                DataSource dataSource = new ByteArrayDataSource(bytesForPdf, "application/pdf");
                MimeBodyPart pdfBodyPart = new MimeBodyPart();
                pdfBodyPart.setDataHandler(new DataHandler(dataSource));
                pdfBodyPart.setFileName("test.pdf");
                mimeMultipart.addBodyPart(pdfBodyPart);
            }

            //create the sender/recipient addresses
            InternetAddress iaSender = new InternetAddress(sender, senderName);
            InternetAddress iaRecipient = new InternetAddress(recipient);

            //construct the mime message
            MimeMessage mimeMessage = new MimeMessage(session);

            mimeMessage.setFrom(iaSender);
            mimeMessage.setSender(iaSender);
            mimeMessage.setSubject(subject);
            mimeMessage.setRecipient(Message.RecipientType.TO, iaRecipient);
            mimeMessage.setContent(mimeMultipart);

            //send off the email
            Transport.send(mimeMessage);
            LOGGER.info("sent from {}, to {} ; server={}, port={} ", sender, recipient, smtpHost, smtpPort);
        } catch (MessagingException | UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}