package com.geos.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@PropertySource("classpath:report.properties")
@Component
public class PdfBytesProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(PdfBytesProducer.class);

    @Value("${report.conceptContract.pagenumbers}")
    private int[] pageNumbers;

    @Value("${report.conceptContract.jspPage}")
    private String jspPage;


    @Value("${report.conceptContract.serverBaseUrl}")
    private String baseUrlUnformatted;

    @Value("${report.conceptContract.pdfTitle}")
    private String pdfTitle;

    @Value("${report.conceptContract.headerTitle}")
    private String headerTitle;

    @Value("${report.conceptContract.pdfKeywords}")
    private String pdfKeywords;


    @Value("${report.conceptContract.pdfAuthor}")
    private String pdfAuthor;

    @Value("${report.conceptContract.pdfCreator}")
    private String pdfCreator;


    @Value("${report.conceptContract.location}")
    private String location;

    @Value("${report.conceptContract.director}")
    private String director;


    public class HeaderFooter extends PdfPageEventHelper {
        public static final String css = "<style>\n" +
                "    table.footer tr td {\n" +
                "        font-size: 8pt;\n" +
                "        font-family: Arial;\n" +
                "    }\n" +
                "\n" +
                "</style>" ;
        public static final String FOOTER_HTML= css+ "<hr /><table class=\"footer\" width=\"100%\" border=\"0\" cellpadding=\"0px\"  cellspacing=\"0\"  >\n" +
                "        <tr>\n" +
                "            <td style=\"width:8cm;\">Paraaf medewerker<br /> </td>\n" +
                "            <td style=\"text-align:center;\">\n" +
                "                Page #CURRENTPAGE/#TOTALPAGES\n" +
                "            </td>\n" +
                "            <td style=\"text-align:right;width:8cm;\"> Paraaf DPA  <br /> </td>\n" +
                "        </tr>\n" +
                "    </table>";


        private int totalPages=1;
        private int currentPage = 1;

        public HeaderFooter(int totalPages) throws IOException {
            super();
            this.totalPages = totalPages;

        }

        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                String footer2Use = "<hr />";
                if (currentPage < totalPages) {
                    footer2Use = FOOTER_HTML.
                            replaceAll("#CURRENTPAGE", String.valueOf(currentPage)).
                            replaceAll("#TOTALPAGES", String.valueOf(totalPages));
                    currentPage++;

                }

                ElementList footer = XMLWorkerHelper.parseToElementList(footer2Use, null);
                ColumnText ct = new ColumnText(writer.getDirectContent());

                ct.setSimpleColumn(new Rectangle(36, 50, 559, 40));
                for (Element e : footer) {
                    ct.addElement(e);
                }
                ct.go();
            } catch (DocumentException de) {
                LOGGER.error(de.getMessage());
                throw new ExceptionConverter(de);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }


    /**
     * The main function to produce PDF as bytes from a request with request params
     *
     * @param params
     * @param request
     * @return
     */
    public byte[] getPDF(Map<String, String> params, HttpServletRequest request) {
        String baseUrl = MiscCalculations.getBaseUrlFromRequest(request);
        String url     =  baseUrl  + baseUrlUnformatted;

        params.put("title"    , headerTitle);
        params.put("plaats"   , location);
        params.put("director" , director);
        params.put("baseUrl"  , baseUrl);

        List<String> htmlData = new ArrayList<>();
        for(int index : pageNumbers) {
            params.put("jspPage", String.format(jspPage, index));
            ResponseEntity<String> result = MiscCalculations.postHashMapToUrl(String.format(url,index), params);
            htmlData.add(result.getBody());
        }


        String[] htmlBody=htmlData.toArray(new String[htmlData.size()]);

        // retrieve contents of "C:/tmp/report.pdf" that were written in showHelp
        byte[] contents = createPdfDataFromHtml(htmlBody);
        return contents;
    }





    /**
     * Using an output Stream the PDF content is writeen for each HTML page
     *
     * @param outputStream
     * @param html
     * @throws DocumentException
     * @throws IOException
     */
    private void writePdf(OutputStream outputStream, String[] html)  {
        //throws DocumentException, IOException
        float left = 30;
        float right = 30;
        float top = 25;
        float bottom = 25;
        Document document =  new Document(PageSize.A4, left, right, top, bottom);

        try {
            PdfWriter pdfWriter =  PdfWriter.getInstance(document, outputStream);
            pdfWriter.setPageEvent(new HeaderFooter(html.length));

            document.open();

            document.addTitle(pdfTitle);
            document.addSubject(headerTitle);
            document.addKeywords(pdfKeywords);
            document.addAuthor(pdfAuthor);
            document.addCreator(pdfCreator);

            XMLWorkerHelper worker=XMLWorkerHelper.getInstance();
            for (int i=0; i < html.length; i++) {
                ByteArrayInputStream is = new ByteArrayInputStream(html[i].getBytes());
                worker.parseXHtml(pdfWriter, document, is);
                document.newPage();
            }
        } catch(DocumentException | IOException e) {
            LOGGER.error(e.getMessage());
        }  finally {
            document.close();
        }
    }

    /**
     * Create one PDF from an array of HTML Strings
     * Each entry will be added to e new page...
     *
     * @param html
     * @return
     */
    private byte[] createPdfDataFromHtml(String[] html) {
        ByteArrayOutputStream outputStream = null;
        byte[] bytes = null;
        try {
            outputStream = new ByteArrayOutputStream();
            writePdf(outputStream, html);
            bytes = outputStream.toByteArray();


        } finally {
            //clean off
            if(null != outputStream) {
                try { outputStream.close(); outputStream = null; }
                catch(Exception ex) { }
            }
        }
        return bytes;
    }


}
