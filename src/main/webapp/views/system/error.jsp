<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <title>Ai ai ai....!! Error</title>
    <spring:url value="/resources/css" var="crunchifyCSS" />
    <spring:url value="/resources/images" var="crunchifyIMG" />
    <link href="${crunchifyCSS}/error.css" rel="stylesheet" type="text/css" />

</head>
<body>
<h1>${error} `${path}`</h1>
<img src="${crunchifyIMG}/errorstop.png"  style="float:left" height="100"/>
Status: ${status} <br />
Message: ${message} <br />
time: ${timestamp} <br />

</body>
</html>
