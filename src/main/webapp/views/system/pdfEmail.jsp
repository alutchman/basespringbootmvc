<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>PDF eMail Contract</title>
    <spring:url value="/resources/css" var="crunchifyCSS" />
    <spring:url value="/resources/js" var="crunchifyJS" />
    <spring:url value="/resources/images" var="crunchifyIMG" />
    <link href="${crunchifyCSS}/base.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${crunchifyCSS}/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="${crunchifyJS}/jquery-1.12.4.js"></script>
    <script src="${crunchifyJS}/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {
            $( "input.datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "d M yy"
            });
        });
    </script>
</head>
<body>
<h2>eMail Concept Contract</h2>
<form method="post">
    <table class="cvCred">
        <tr>
            <td>Aanhef</td><td>Voornaam</td><td> Achternaam</td>
        </tr>
        <tr>
            <td  colspan="1">
                <select name="aanhef">
                    <option value="De Heer">De Heer</option>
                    <option value="Mevrouw">Mevrouw</option>
                </select>
            </td>
            <td> <input type="text" name="firstname"  value=""   size="10" /></td>
            <td> <input type="text" name="lastname"  value="" size="20" /></td>

        </tr>
        <tr>
            <td>&nbsp;</td><td>eMail</td><td>Telefoon</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td> <input type="text" name="email" size="20" value=""/> </td>
            <td> <input type="number" name="telefoon"  value="" pattern="[0-9]*"   size="10" /> </td>
        </tr>
        <tr>
            <td>Basisloon</td><td> Werkuren</td><td>Vakantiedagen</td>
        </tr>
        <tr>
            <td>
                <select name="wage">
                    <c:forEach var="aWage" items="${wage}">
                        <option value="${aWage}" <c:if test="${aWage eq 2500}">selected="selected"</c:if>>${aWage}</option>
                    </c:forEach>
                </select>
            </td>
            <td>
                <select name="werkuren">
                    <c:forEach var="aWerkuren" items="${werkuren}">
                        <option value="${aWerkuren}">${aWerkuren}</option>
                    </c:forEach>
                </select>
            </td>
            <td>
                <select name="vakantiedagen">
                    <c:forEach var="aVakantiedagen" items="${vakantiedagen}">
                        <option value="${aVakantiedagen}" <c:if test="${aVakantiedagen eq 25}">selected="selected"</c:if>>${aVakantiedagen}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>

    </table>
    <table>
        <tr>
            <td>Geboortedatum:</td><td><input type="text" class="datepicker" name="geboorteDatum" value="" /></td>
        </tr>
        <tr>
            <td>Gewenste aanvangdatum:</td><td><input type="text" class="datepicker" name="aanvangdatum" value="" /></td>
        </tr>
        <tr>
            <td>Gewenste ondertekendatum:</td><td><input type="text" class="datepicker" name="ondertekendatum" value="" /></td>
        </tr>
        <tr>
            <td>&nbsp;</td><td><input type="submit"  value="email contract" /></td>
        </tr>
    </table>
</form>
<hr />
Current context: '${pageContext.servletContext.contextPath}' <br />
</body>
</html>
