<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>PDF eMail Contract</title>
    <spring:url value="/resources/css" var="crunchifyCSS" />
    <spring:url value="/resources/js" var="crunchifyJS" />
    <spring:url value="/resources/images" var="crunchifyIMG" />
    <link href="${crunchifyCSS}/base.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${crunchifyCSS}/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="${crunchifyJS}/jquery-1.12.4.js"></script>
    <script src="${crunchifyJS}/jquery-ui.js"></script>
    <script>
        $(document).ready(function() {

        });
    </script>
</head>
<body>
   <table>
       <tr>
           <td>
               <img src="${crunchifyIMG}/DPA-GEOS.jpg"  height="50px"/>
           </td>
           <td>
               Bedankt voor het opvragen van het concept contract.
           </td>
       </tr>

   </table>

</body>
</html>