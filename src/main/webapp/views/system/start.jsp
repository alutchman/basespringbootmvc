<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Spring Boot Hello World Example with JSP</title>
    <spring:url value="/resources/css" var="crunchifyCSS" />
    <spring:url value="/resources/images" var="crunchifyIMG" />
    <link href="${crunchifyCSS}/base.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h2>Spring Boot Hello World Example with JSP</h2>
    <c:if test="${not empty features}">

        <ul>
            <c:forEach var="listValue" items="${features}">
                <li>${listValue}</li>
            </c:forEach>
        </ul>

    </c:if>
<hr />
<ol>
    <li><a href="/contract/email.html" >email contract</a></li>
    <li><a href="example.pdf" >Dummy contract als PDF</a></li>
    <li><a href="company.xml" >XML with prefix dpa</a></li>
</ol>
</body>
</html>
