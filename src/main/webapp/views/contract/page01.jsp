<div class="page">
    <%@ include file="header.jsp" %>
    <h1>ARBEIDSOVEREENKOMST VOOR Onbepaalde tijd </h1>

    <strong>De ondergetekenden:</strong>&nbsp;<br />
    &nbsp;<br />
    <table>
        <tr>
            <td style="width:1cm;">1.</td>
            <td style="width:100%;">
                De besloten vennootschap met beperkte aansprakelijkheid DPA Beheer, gevestigd te
                Amsterdam te dezer zake rechtsgeldig vertegenwoordigd door ${director},
                hierna te noemen "DPA";
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                en
            </td>
        </tr>
        <tr>
            <td>2.</td>
            <td>
                ${aanhef}  ${firstname} ${lastname}, geboren op ${geboorteDatum}, wonende te (Postcode) Woonplaats, Straat
                huisnummer, hierna te noemen "de medewerker";
                &nbsp;<br />
                &nbsp;<br />
                gezamenlijk te noemen: de "partijen",
            </td>
        </tr>
    </table>
    &nbsp;<br />
    &nbsp;<br />
    <strong>Verklaren het navolgende te zijn overeengekomen: </strong>
    &nbsp;<br />
    &nbsp;<br />

    <span class="undelinedBold">Artikel 1</span><br />
    <span class="undelinedBold">Indiensttreding </span>
    <ol>
        <li>De medewerker treedt bij DPA in dienst in de functie van (Functie). De functie en verantwoordelijkheden van de
            medewerker kunnen door DPA binnen de grenzen van de redelijkheid tijdens het dienstverband worden gewijzigd
            indien het belang van een goede bedrijfsvoering dit naar het oordeel van DPA vergt en de medewerker
            verklaart zich op voorhand bereid in een dergelijk geval die andere werkzaamheden of taken te verrichten.
            Dit kan bijvoorbeeld het geval zijn indien er sprake is van een wijziging van de organisatie binnen de onderneming.
        </li>
        <li>
            De werkzaamheden worden door de medewerker persoonlijk verricht, naar beste vermogen, volgens de aanwijzingen en instructie van DPA.
        </li>
        <li>
            De standplaats van de medewerker is bij aanvang van de arbeidsovereenkomst (Standplaats). De medewerker wordt als
            interim professional uit hoofde van de arbeidsovereenkomst bij opdrachtgevers van DPA gedetacheerd.
            De medewerker kan, gezien zijn functie, worden ingezet voor het landelijke opdrachtgeversnetwerk van DPA.
        </li>
    </ol>

    <span class="undelinedBold">Artikel 2</span><br />
    <span class="undelinedBold">Aanvang en einde dienstverband </span>
    <ol  style="padding-left:10px;">
        <li>De arbeidsovereenkomst vangt aan op ${aanvangdatum} en is aangegaan voor onbepaalde tijd. </li>
        <li>De arbeidsduur per week bedraagt ${werkuren} uur. DPA kan van de medewerker verlangen overwerk te verrichten, indien het belang van DPA dit vereist. </li>
        <li>Deze arbeidsovereenkomst kan door de medewerker en DPA worden opgezegd met inachtneming van een opzegtermijn van &eacute;&eacute;n kalendermaand. </li>
        <li>Opzegging dient schriftelijk te geschieden tegen het einde van een kalendermaand. </li>
        <li>De arbeidsovereenkomst eindigt van rechtswege, zonder dat opzegging of een andere handeling is vereist, op de dag waarop de medewerker de voor hem geldende AOW-gerechtigde leeftijd bereikt.</li>
        <li>De eerste twee maanden van het dienstverband geldt voor beide partijen als proeftijd.</li>
    </ol>
</div>
