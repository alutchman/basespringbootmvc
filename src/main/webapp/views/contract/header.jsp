<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:url value="${baseUrl}/resources/css" var="crunchifyCSS" />
<spring:url value="${baseUrl}/resources/images" var="crunchifyIMG" />
<link href="${crunchifyCSS}/pdf.css" rel="stylesheet" type="text/css" />
<table class="header">
    <tr>
        <td><img src="${crunchifyIMG}/DPA-GEOS.jpg"  height="1cm"  /></td>
        <td style="text-align: right;">${title}</td>
    </tr>
</table>
