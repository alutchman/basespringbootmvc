<div class="page">
	<%@ include file="header.jsp" %>
	<span class="undelinedBold">Artikel 11</span><br />
	<span class="undelinedBold">Boete</span>
	<p>
	Bij overtreding van het bepaalde in de artikelen 8 tot en met 10, is DPA, bevoegd zonder dat hiertoe
	enige voorafgaande kennisgeving of ingebrekestelling vereist is, indien en voor zover nodig in afwijking van
	het bepaalde in artikel 7:650 lid 3 BW, aan de medewerker een onmiddellijk opeisbare boete ten bedrage
	van respectievelijk &euro; 15.000,- per overtreding alsmede een boete van &euro; 1.000,- per dag dat de overtreding
	voortduurt, op te leggen. Onverminderd de aanspraak van DPA op bovenstaande boete kan de medewerker,
	in afwijking van het bepaalde in artikel 6:92 lid 1,2, en 3 BW gehouden worden tot nakoming van
	de artikelen 7 tot en met 10 en/of (voor zover wettelijk voorgeschreven in plaats van de boete)
	tot volledige schadevergoeding ter zake van enige overtreding.
	</p>

	<span class="undelinedBold">Artikel 12</span><br />
	<span class="undelinedBold">Rechten van industri&euml;le en intellectuele eigendom </span>
	<ol>
		<li>DPA komt alle intellectuele eigendomsrechten toe, welke zijn ontstaan in het kader van door de medewerker bij de uitvoering
		van de arbeidsovereenkomst ontwikkelde gegevens, resultaten, adviezen, rapporten, documentatie, programma's, werkinstructies
		en overige materialen ongeacht de vorm daarvan, alsmede van de daarin vervatte informatie, welke hierna verder genoemd worden: "Werken".
		</li>
		<li>Onder intellectuele eigendomsrechten in deze arbeidsovereenkomst worden verstaan: alle geregistreerde of niet geregistreerde
		intellectuele eigendomsrechten die waar dan ook ter wereld van tijd tot tijd bestaan, met inbegrip van auteursrechten, knowhow,
		handelsmerken, octrooien, modelrechten en databankrechten. Het omvat eveneens een aanvraag voor dergelijke rechten.
		</li>
		<li>De medewerker draagt hierbij bij voorbaat alle rechten aan DPA over met betrekking tot de onder lid 1 bedoelde Werken,
		zonder dat medewerker jegens DPA aanspraak kan maken op enige vergoeding voor de overdracht van (het eigendom) van deze rechten.
		</li>
		<li>Indien voor de overdracht van de in dit artikel bedoelde materialen en daarmee samenhangende rechten enige handeling,
		akte of andersoortige formaliteit vereist is van/door de medewerker, verplicht de medewerker zich hierbij bij voorbaat
		DPA al de nodige assistentie te bieden, welke voor deze overdracht noodzakelijk is.
		</li>
		<li>De medewerker erkent dat een redelijke compensatie voor het feit dat intellectuele en industri&euml;le eigendomsrechten
		toe zullen vallen aan DPA van rechtswege of door overdracht aan DPA - zoals verwoord in lid 3 van dit artikel -
		is verdisconteerd in het in artikel 3 bedoelde salaris.
		</li>
	</ol>

	<span class="undelinedBold">Artikel 13</span><br />
	<span class="undelinedBold">Teruggave verplichting </span>
	<p>In geval van arbeidsongeschiktheid en (on)betaald verlof langer dan 1 maand, in geval van op non- actiefstelling en in geval
	van be&euml;indiging van de arbeidsovereenkomst dient de medewerker alle zaken en/of schriftelijke stukken, waaronder mede
	begrepen geacht moet worden andersoortige informatiedragers en andere eigendommen van DPA die hij nog onder zich heeft,
	onverwijld en onvoorwaardelijk schoon en onbeschadigd aan DPA ter beschikking te stellen, zonder daarvoor enige
	vergoeding te bedingen en zonder daarvan kopieën te behouden.
	</p>

	<span class="undelinedBold">Artikel 14</span><br />
	<span class="undelinedBold">Scholing</span>
	<ol>
		<li>DPA stelt de medewerker in staat scholing te volgen die noodzakelijk is voor de uitoefening van zijn functie, alsmede,
			indien DPA daartoe redelijkerwijs aanleiding ziet,	 scholing die erop gericht is de arbeidsmobiliteit van de medewerker te behouden c.q. te vergroten.
		</li>
		<li>De medewerker stemt er mee in dat DPA, indien en voor zover op grond van wet-en regelgeving toegestaan,
			de voor haar met de in dit artikel bedoelde scholing gepaard gaande kosten in mindering zal brengen op
			de eventuele transitievergoeding waarop de medewerker ex artikel 7:673 BW aanspraak kan maken.
		</li>
	</ol>
</div>