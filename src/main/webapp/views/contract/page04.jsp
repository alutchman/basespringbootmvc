<div class="page">
	<%@ include file="header.jsp" %>
	<span class="undelinedBold">Artikel 9</span><br />
	<span class="undelinedBold">Relatiebeding en concurrentiebeding </span><br />
	<table>
	  <tr>
		<td style="width:1cm;">1.</td> <td  style="width:100%;">Relatiebeding &nbsp; </td>
	  </tr>
		<tr>
		<td>&nbsp;</td>
		<td>Het is de medewerker niet toegestaan om, zonder schriftelijke toestemming van DPA, gedurende het dienstverband met DPA of in de periode
		 van &eacute;&eacute;n jaar nadien, hetzij zelfstandig voor eigen rekening en risico, hetzij in dienst van, ten behoeve van of namens derden, op enigerlei
		 wijze zakelijke betrekkingen aan te gaan of te onderhouden met relaties van DPA en/of aan DPA gelieerde vennootschappen.
		 Onder relaties van DPA dienen ook te worden verstaan personen, bedrijven of instellingen, bij wie de medewerker feitelijk gedetacheerd
		 is geweest (de "inlener"), ook indien dat in subdetachering (doorlenen) is geweest.
		</td>
	  </tr>
	  <tr>
		<td>2.</td> <td>Concurrentiebeding &nbsp; </td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td>Het is de medewerker zonder schriftelijke toestemming van DPA verboden om zowel tijdens het dienstverband met DPA als
		gedurende een periode van &eacute;&eacute;n jaar - ongeacht de wijze waarop en de redenen waarom het dienstverband tot een einde is gekomen - na be&euml;indiging
		van de arbeidsovereenkomst in enigerlei vorm, direct of indirect, hetzij tegen vergoeding, hetzij om niet werkzaam of betrokken te zijn in of
		bij enige onderneming, welke onderneming activiteiten verricht op een terrein gelijk, gelijksoortig of aanverwant aan of anderszins concurrerend
		 met de onderneming van DPA, te weten een onderneming die zich bezighoudt met de bemiddeling van ICT geschoolde professionals, noch daarbij
		 zijn bemiddeling, in welke vorm dan ook, direct of indirect te verlenen of daarin aandeel van welke aard ook te hebben.
		</td>
	  </tr>
	  <tr>
		<td>3.</td> <td>Wijziging &nbsp; </td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td>De medewerker is verplicht om zijn medewerking te verlenen aan aanpassingen van het in lid 1 en lid 2 van dit artikel opgenomen relatie- en
		concurrentiebeding indien dit op grond van veranderingen in de wetgeving of wijzigingen in of van de functie van de medewerker noodzakelijk is.
		</td>
	  </tr>
	</table>

	<span class="undelinedBold">Artikel 10</span><br />
	<span class="undelinedBold">Geheimhouding en "verbod om medewerkers mee te nemen" </span><br />
	<table>
	  <tr>
		<td style="width:1cm;">1.</td>
		 <td  style="width:100%;">De medewerker erkent dat hem door DPA geheimhouding is opgelegd van alle bijzonderheden met betrekking tot het bedrijf van DPA en/of aan DPA
		gelieerde vennootschappen of daarmee verband houdende. Het is de medewerker derhalve verboden, hetzij gedurende het dienstverband, hetzij na
		het einde hiervan, op enigerlei wijze aan derden, direct of indirect, enige mededeling te doen aangaande enige bijzonderheden van het bedrijf van DPA
		of met betrekking tot de opdrachtgevers of relaties van DPA en/of aan DPA gelieerde vennootschappen of daarmee verband houdende.
		 </td>
	  </tr>
	  <tr>
		<td>2.</td>
		<td>Onverminderd het voorgaande is het de medewerker niet toegestaan om zonder voorafgaande schriftelijke toestemming van DPA informatie betrekking
		hebbende op DPA en/of aan DPA gelieerde vennootschappen, aan derden te verstrekken of buiten het bedrijf van DPA te (doen) brengen.
		</td>
	  </tr>
	  <tr>
		<td>3.</td>
		<td>Het is de medewerker niet toegestaan om zowel tijdens het dienstverband als gedurende een periode van twee jaar - ongeacht
		de wijze waarop en de redenen waarom het dienstverband tot een einde is gekomen - na be&euml;indiging van de arbeidsovereenkomst,
		direct dan wel indirect, medewerkers van DPA en/of van aan DPA gelieerde vennootschappen in dienst te nemen.
		Ook is het benaderen van medewerkers van DPA en/of aan DPA gelieerde vennootschappen, teneinde hen te bewegen het
		dienstverband met DPA en/of aan DPA gelieerde vennootschappen te be&euml;indigen, niet toegestaan.
		</td>
	  </tr>
	  <tr>
		<td>4.</td>
		<td>Onder "gelieerde vennootschappen" in deze arbeidsovereenkomst worden in ieder geval verstaan vennootschappen die
		behoren tot hetzelfde concern of dezelfde groep als DPA, alsmede vennootschappen waarin DPA op enigerlei wijze participeert.
		</td>
	 </tr>
	</table>
</div>