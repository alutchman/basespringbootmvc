<div class="page">
    <%@ include file="header.jsp" %>
    <span class="undelinedBold">Artikel 3</span><br />
    <span class="undelinedBold">Salaris</span>
    <ol>
    <li>Het basissalaris bedraagt &euro; ${wage} bruto per maand op basis van een ${werkuren}-urige werkweek. Het maandsalaris wordt telkens aan het einde van de
        betreffende maand uitgekeerd onder aftrek van de wettelijke inhoudingen. De medewerker kan geen aanspraak maken op betaling van het maandsalaris
        v&oacute;&oacute;r het einde van de maand.</li>
    <li>Overwerk komt alleen voor vergoeding in aanmerking indien DPA (en opdrachtgever van DPA indien aan de orde) daarvoor toestemming heeft gegeven.
        De vakantietoeslag bedraagt 8% per jaar, berekend over het hiervoor genoemde basissalaris. De referteperiode loopt van 1 juli tot en met 30 juni.
        De vakantietoeslag wordt jaarlijks in de maand juni uitbetaald. Als de medewerker in de maand juni nog geen jaar in dienst is, heeft hij recht op
        een pro rata gedeelte van de vakantietoeslag.</li>
    <li> Studiekosten zullen na overleg met DPA enkel en alleen conform de tussen partijen gesloten studieovereenkomst worden vergoed.</li>
    <li>Op jaarbasis verkrijgt de medewerker (interim professional) bij een fulltime dienstverband, volledige inzetbaarheid en geen ziektedagen een
        bruto Niet-Ziek-Zijn-bonus van maximaal &euro; 700,- per jaar. Per ziektedag reduceert deze bonus met 10% van de maximale bonus die van toepassing
        is op de medewerker (interim professional). Deze bonus wordt uitbetaald in de maand juli over de daaraan voorafgaande periode van 1 juli t/m 30 juni.
        Voorwaarde voor uitbetaling is het in dienst zijn van de medewerker bij DPA op 30 juni van het jaar van uitbetaling. De bonus wordt naar rato herrekend
        naar het aantal werkzame dagen op opdracht en aantal dagen dienstverband.</li>
    <li>De medewerker (interim professional) neemt zesentwintig weken na aanvang van het dienstverband deel aan de StiPP pensioenregeling.
        De medewerker verklaart door ondertekening van deze overeenkomst dat hij van de inhoud heeft kennis genomen en daarmee akkoord gaat.
        Eventueel toekomstige wijzigingen in de pensioenregeling (tussen DPA en het pensioenfonds) zullen rechtstreeks doorwerken en van toepassing
        zijn op de arbeidsovereenkomst met de medewerker.</li>
    </ol>
    <span class="undelinedBold">Artikel 4</span><br />
    <span class="undelinedBold">Autoregeling</span>
    <span>
    Indien aan de medewerker in het kader van de uitoefening van zijn functie een auto ter beschikking wordt gesteld, geldt de Autoregeling DPA.
    De medewerker verklaart deze regeling te kennen en zich daaraan te conformeren. Deze regeling maakt onderdeel uit van deze arbeidsovereenkomst.
    </span>
    &nbsp;<br />
    &nbsp;<br />
    <span class="undelinedBold">Artikel 5</span><br />
    <span class="undelinedBold">Reiskostenvergoeding en kostenvergoeding</span>
    <ol>
        <li>Indien aan de medewerker geen auto ter beschikking wordt gesteld, worden de kosten woon - werkverkeer vergoed op basis van 2e klas openbaar vervoer onder overlegging van originele vervoersbewijzen of worden de in de eigen auto van de medewerker gereden kilometers vergoed op basis van een kilometervergoeding. Voornoemde kilometervergoeding wordt jaarlijks door de directie vastgesteld. De vergoeding bedraagt thans &euro; 0,19 cent per daadwerkelijk gereden kilometer. De vergoeding wordt alleen indien en voor zover fiscaal mogelijk, netto uitgekeerd. In de kilometervergoeding wordt een door de medewerker af te sluiten, voldoende dekkende casco-, inzittenden- en ongevallenverzekering geacht te zijn begrepen. Aan de medewerker opgelegde verkeers- of parkeerboetes worden niet door DPA vergoed.</li>
        <li>DPA zal maandelijks de door de medewerker in het belang van DPA gemaakte kosten vergoeden op basis van door de medewerker ingediende gespecificeerde declaraties en bescheiden, een en ander uitsluitend indien en voor zover voor het maken van deze onkosten voorafgaande toestemming van DPA is verkregen.  </li>
    </ol>
</div>