<div class="page">
	<%@ include file="header.jsp" %>

    <span class="undelinedBold">Artikel 6</span><br />
    <span class="undelinedBold">Vakantiedagen</span><br />
	<span>Buiten de algemeen erkende feestdagen heeft de medewerker bij een fulltime dienstverband recht op ${vakantiedagen} vakantiedagen per jaar, conform de DPA CAO.
	Indien de arbeidsovereenkomst minder dan een jaar bedraagt, zal het aantal vakantie-uren pro rato worden berekend, afgerond op halve uren, in het voordeel van de medewerker.
	De opname van een vakantieperiode wordt in onderling overleg en met goedkeuring van de opdrachtgever (en DPA) bepaald. In beginsel moeten alle vakantiedagen waarop
	de medewerker recht heeft worden opgenomen in het kalenderjaar waarin ze zijn opgebouwd.
	<br />
	Indien de medewerker bij het einde van de arbeidsovereenkomst een negatief vakantiedagentegoed heeft, zal DPA dit negatieve vakantiedagentegoed
	bij de eindafrekening van de medewerker verrekenen.
	</span>
    &nbsp;<br />
    &nbsp;<br />
	<span class="undelinedBold">Artikel 7</span><br />
	<span class="undelinedBold">Arbeidsongeschiktheid</span><br />
	<span>Onverminderd de bepalingen in de DPA CAO en Personeelsregeling met betrekking tot arbeidsongeschiktheid geldt het volgende: </span>
	<ol>
        <li>
            Indien de loondoorbetalingsverplichting van DPA ook na 104 weken van arbeidsongeschiktheid voortduurt, heeft de medewerker slechts
            aanspraak op hetgeen DPA op grond van de wet gedurende het tweede tijdsvak van 52 weken van arbeidsongeschiktheid zou moeten betalen.
        </li>
        <li>
            Bij niet naleving van de (reïntegratie-)voorschriften kan DPA sancties treffen waaronder - doch daartoe niet beperkt - het
            geven van een waarschuwing, opschorten c.q. stopzetten van salaris, of in ernstige gevallen ook ontslag.
        </li>
        <li>
            Als de medewerker bij het einde van de arbeidsovereenkomst arbeidsongeschikt is wegens ziekte, dan wel binnen 4 weken na
            het einde van de arbeidsovereenkomst arbeidsongeschikt wordt wegens ziekte, en op dat moment niet in dienst is van een andere werkgever of
            een WW-uitkering geniet, is hij verplicht:
              <ul>
                <li>DPA schriftelijk op de hoogte te stellen van het ontstaan van de arbeidsongeschiktheid, tenzij de
                arbeidsongeschiktheid bij het einde van de arbeidsovereenkomst al bestond;
                </li>
                <li>gehoor te geven aan een oproep van de bedrijfsarts en/of arbeidsdeskundige van DPA;
                </li>
                <li>aan DPA alle informatie te verstrekken die hij op grond van de Ziektewet of de Wet Werk en Inkomen naar
                Arbeidsvermogen (Wet WIA) aan het UWV dient te verstrekken. Indien de medewerker geen toestemming
                geeft medische gegevens aan	DPA te verstrekken dient hij deze wel aan een bedrijfsarts of arts-gemachtigde te verstrekken;
                </li>
                <li>alle verplichtingen na te komen die volgen uit de Ziektewet en de Wet WIA, waaronder de informatieverplichtingen aan
                    het UWV, DPA en/of een bedrijfsarts of arts-gemachtigde;
                </li>
                <li>mee te werken aan een namens DPA aangeboden re-integratietraject of proefplaatsing; en</li>
                <li>een (vervroegde) IVA-uitkering aan te vragen indien en zodra de behandelend bedrijfsarts dit mogelijk acht.</li>
              </ul>
        </li>
        <li>De in de voorgaande alinea opgenomen verplichtingen blijven bestaan zolang de medewerker arbeidsongeschikt blijft
         en een Ziektewetuitkering geniet. Is de medewerker volledig hersteld, dan eindigen de verplichtingen, tenzij de
         medewerker binnen 4 weken na herstelmelding opnieuw arbeidsongeschikt raakt. In dat geval herleven de verplichtingen
         zoals vermeld in de voorgaande alinea.</li>
	</ol>

	<span class="undelinedBold">Artikel 8</span><br />
	<span class="undelinedBold">Nevenactiviteiten</span>
	<p style="page-break-after:always">
	Tijdens het dienstverband zal de medewerker zonder toestemming van DPA geen (al dan niet gehonoreerde) nevenactiviteiten verrichten.
	</p>
</div>
