<div class="page">
    <%@ include file="header.jsp" %>
    <span class="undelinedBold">Artikel 15</span><br />
    <span class="undelinedBold">DPA CAO, Personeelsregeling DPA en overige reglementen </span>
    <p>Op deze arbeidsovereenkomst zijn de bepalingen van de DPA CAO, de Personeelsregeling DPA, de Autoregeling DPA en overige (eventuele)
     reglementen van toepassing. De bepalingen van de DPA CAO, de Personeelsregeling DPA, de Autoregeling DPA en de overige (eventuele) reglementen
     maken deel uit van deze overeenkomst. Door ondertekening van deze overeenkomst verklaart de medewerker op de hoogte te zijn van voornoemde regelingen
     en met de inhoud daarvan akkoord te gaan. Op deze arbeidsovereenkomst zijn de bepalingen van de van tijd tot tijd geldende DPA CAO, de Personeelsregeling DPA,
     en overige (in deze arbeidsovereenkomst genoemde) reglementen van toepassing. Deze regelingen maken, indien en voor zover van toepassing,
     integraal onderdeel uit van deze arbeidsovereenkomst.. Door ondertekening van deze overeenkomst verklaart de medewerker een exemplaar daarvan
     te hebben ontvangen en op de hoogte te zijn van voornoemde regelingen. De medewerker verklaart zich met de inhoud en eventuele toekomstige
     wijzigingen en/of aanpassingen daarvan akkoord.
    Wanneer de regelingen zoals opgenomen in de Personeelsregeling afwijken van deze arbeidsovereenkomst, prevaleert deze arbeidsovereenkomst.</p>

    <span class="undelinedBold">Artikel 16</span><br />
    <span class="undelinedBold">Wijziging overeenkomst </span>
    <p>DPA behoudt zich het recht voor om met inachtneming van artikel 7:613 BW de bepalingen in deze arbeidsovereenkomst c.q.
     de arbeidsvoorwaarden (eenzijdig) te wijzigen c.q. aan te vullen, indien de omstandigheden daartoe redelijkerwijs aanleiding geven.
     Indien een wetswijziging daartoe aanleiding geeft, zal DPA ook (waar nodig) eenzijdig wijzigingen kunnen aanbrengen in de arbeidsovereenkomst.
    </p>

    <span class="undelinedBold">Artikel 17</span><br />
    <span class="undelinedBold">Geldigheidsduur</span>
    <p>Als deze arbeidsovereenkomst niet binnen 10 werkdagen, na ondertekening en dagtekening door DPA, door de medewerker is
        ondertekend en geretourneerd aan DPA, dan komt deze overeenkomst te vervallen en kan de medewerker geen rechten meer ontlenen aan de inhoud ervan.
    </p>

    <br />
    &nbsp;<br />

    <span class="undelinedBold">Artikel 18</span><br />
    <span class="undelinedBold">Rechtskeuze en forumkeuze </span>
    <ol>
        <li>Op deze arbeidsovereenkomst is Nederlands recht van toepassing. </li>
        <li>De Nederlandse rechter is bij uitsluiting bevoegd om van geschillen direct of indirect voortvloeiende uit deze arbeidsovereenkomst kennis te nemen. </li>
        <li>Deze arbeidsovereenkomst vervangt alle eerdere (mondelinge dan wel schriftelijke) afspraken tussen partijen.
            Wijzigingen en/of aanpassingen van deze arbeidsovereenkomst zijn alleen bindend wanneer zij schriftelijk door DPA aan de medewerker zijn bevestigd.
        </li>
    </ol>

    <p  class="spacer">&nbsp;<br /></p>

    <p>
        Aldus opgemaakt in tweevoud, per bladzijde geparafeerd en door partijen ondertekend te ${plaats}  op ${ondertekendatum}.
    </p>

    <table style="width:15cm;">
        <tr>
            <td style="width:45%;">DPA Beheer BV</td> <td> &nbsp; </td><td style="width:45%;"> De Medewerker </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>

        <tr>
            <td style="border-bottom:1px solid black;">&nbsp;</td>  <td> &nbsp; </td> <td style="border-bottom:1px solid black;"> &nbsp; </td>
        </tr>
        <tr>
            <td>${director}</td> <td> &nbsp; </td><td> ${aanhef}  ${firstname} ${lastname}</td>
        </tr>
    </table>

</div>
